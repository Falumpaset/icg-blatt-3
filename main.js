let gl;
let program;
let objects = [];
let posLoc,
	colorLoc,
	modelLoc,
	viewLoc;
let viewMat;
let cameraPosition = [1, 0, 3],
	cameraTarget = [0, 0, 0],
	cameraDirection = [0, 0, 0],
	up = [0, 1, 0],
	cameraRight = [0, 0, 0],
	center = [0, 0, 0];
let lastX,
	lastY,
	yaw = 0.0,
	pitch = 0.0;
	let firstMouse =true;

// TODO: 1.4 + 2.4: Führe globale Variablen ein für Werte, die in verschiedenen Funktionen benötigt werden


function main() {

	// Get canvas and setup WebGL context
	const canvas = document.getElementById("gl-canvas");
	gl = canvas.getContext('webgl2');

	// Configure viewport
	gl.viewport(0, 0, canvas.width, canvas.height);
	gl.clearColor(1.0, 1.0, 1.0, 1.0);

	gl.enable(gl.DEPTH_TEST);

	// Init shader program via additional function and bind it
	program = initShaders(gl, "vertex-shader", "fragment-shader");
	gl.useProgram(program);

	// Get locations of shader variables
	posLoc = gl.getAttribLocation(program, "vPosition");
	colorLoc = gl.getAttribLocation(program, "vColor");
	// TODO 1.3 + 2.3: Bestimme Locations der Shadervariablen für Model und View Matrix

	modelLoc = gl.getUniformLocation(program, "modelMat");
	viewLoc = gl.getUniformLocation(program, "viewMat");


	// TODO 2.5: Erstelle mithilfe der Funktionen aus gl-matrix.js eine initiale View Matrix

	setUpViewMatrix();






	// TODO 2.6: Übergebe die initiale View Matrix an den Shader


	// TODO 2.8: Füge einen Event Listener für Tastatureingaben hinzu

	document.addEventListener('keypress', (e) => {
		let cameraSpeed = [0.02, 0.02, 0.02];
		let update = [0, 0, 0];
		switch (e.key) {
			case "w":

				cameraPosition = vec3.subtract(cameraPosition, cameraPosition, vec3.multiply(update, cameraSpeed, cameraDirection));
				break;
			case "s":
				cameraPosition = vec3.add(cameraPosition, cameraPosition, vec3.multiply(update, cameraSpeed, cameraDirection));
				break;

			case "a":

				update = vec3.multiply(update, cameraRight, cameraSpeed);
				cameraTarget = vec3.sub(cameraTarget, cameraTarget, update);
				console.log("1:" + cameraDirection);


				cameraPosition = vec3.sub(cameraPosition, cameraPosition, update);
				updateCameraDirection();
				console.log("2:" + cameraDirection);
				console.log("--------------------------------------");

				break;

			case "d":
				update = vec3.multiply(update, cameraRight, cameraSpeed);
				cameraTarget = vec3.add(cameraTarget, cameraTarget, update);
				console.log("1:" + cameraDirection);


				cameraPosition = vec3.add(cameraPosition, cameraPosition, update);
				updateCameraDirection();
				console.log("2:" + cameraDirection);
				console.log("--------------------------------------");
				break;

			default:
				break;
		}
		updateViewMat();
	});

	document.addEventListener('mousemove', (e) => {
		mouseCallBack(e.clientX, e.clientY);
	});




	// Create object instances


	// TODO 1.5: Erstelle mehrere Baum-/Wolkeninstanzen und einen Fluss

	createObjects("island", 1);
	createObjects("cloud", 3);
	createObjects("tree", 1);
	//createObjects("river", 1);


	// TODO 1.9: Rufe für jedes Objekt die Methode 'SetModelMatrix' auf und 
	// cameraPositioniere das Objekt auf diese Weise auf der Insel

	pushModelMatrixObjects();
	render();

};

 
function mouseCallBack(xpos, ypos) {

	if(firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

	let xoffset = xpos - lastX;
	let yoffset = lastY - ypos;
	lastX = xpos;
	lastY = ypos;

	let sensitivity = 0.5;
	xoffset *= sensitivity;
	yoffset *= sensitivity;

	yaw += xoffset;
	pitch += yoffset;

	if (pitch > 89.0) {
		pitch = 89.0;
	}
	if (pitch < -89.0) {
		pitch = -89.0;
	}
	let target= [0,0,0];
	
	target[0] = Math.cos(glMatrix.toRadian(yaw)) * Math.cos(glMatrix.toRadian(pitch));
	target[1]= Math.sin(glMatrix.toRadian(pitch));
	target[2] = Math.sin(glMatrix.toRadian(yaw)) * Math.cos(glMatrix.toRadian(pitch));
	cameraTarget= target;

	updateCameraDirection();

	updateViewMat();

}

function setUpViewMatrix() {

	viewMat = mat4.create();

	updateCameraDirection();

	updateCameraRight();

	updateViewMat();
}

function updateViewMat() {
	//center = vec3.add(center, cameraPosition, cameraDirection);
	mat4.lookAt(viewMat, cameraPosition, cameraTarget, up);
	gl.uniformMatrix4fv(viewLoc, false, viewMat);
}

function updateCameraDirection() {

	cameraDirection = vec3.subtract(cameraDirection, cameraPosition, cameraTarget);
	cameraDirection = vec3.normalize(cameraDirection, cameraDirection);
	return cameraDirection;
}


function updateCameraRight() {
	cameraRight = vec3.cross(cameraRight, up, cameraDirection);
	cameraRight = vec3.normalize(cameraRight, cameraRight);
	return cameraRight;
}
function instanceCheck(object) {
	let cameraPosition;
	let scale;
	let orientation;
	if (object instanceof Tree) {
		cameraPosition = [-0.2, 0.2, -0.5];
		scale = [0.1, 0.1, 0.1];
		orientation = [0, 0, 0];
	}
	else if (object instanceof Island) {
		cameraPosition = [0.0, 0.0, 0.0];
		scale = [0.2, 0.2, 1];
		orientation = [0, 0, 0];
	}
	else if (object instanceof Cloud) {
		cameraPosition = [Math.random(), 0.1, Math.random()];
		scale = [0.05, 0.1, 1];
		orientation = [0, Math.random() * 360, 0];
	}
	else if (object instanceof River) {
		cameraPosition = [0, 0.1, 0];
		scale = [0.05, 0.1, 1];
		orientation = [0, 0, 0];
	}
	object.SetModelMatrix(cameraPosition, scale, translateToDeg(orientation));
}

function translateToDeg(array) {
	let transform = [];
	for (let index = 0; index <= array.length; index++) {
		transform[index] = glMatrix.toRadian(array[index]);
	}
	return transform;
}

function pushModelMatrixObjects() {
	objects.forEach(object => {

		instanceCheck(object);
	});
}

function determineObject(input) {
	switch (input) {
		case "island":
			return Island;
		case "cloud":
			return Cloud;
		case "tree":
			return Tree;
		case "river":
			return River;

	}
}
function createObjects(objectName, count) {
	for (let index = 1; index <= count; index++) {
		let object = determineObject(objectName);
		objects.push(new object());
	}
}


function render() {

	// Only clear once
	gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
	// Call render function of each scene object
	for (let object of objects) {

		object.Render();
	};

	requestAnimationFrame(render);
}

// TODO 2.7: Erstelle einen Event-Handler, der anhand von WASD-Tastatureingaben
// die View Matrix anpasst
function move(e) {

}

main();
